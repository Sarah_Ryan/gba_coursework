
//GBA Library includes
#include <gba_console.h>
#include <gba_video.h>
#include <gba_interrupt.h>
#include <gba_systemcalls.h>
#include <gba_input.h>
#include <maxmod.h> //Maxmod lib
#include "soundbank.h" //Sound file
#include "soundbank_bin.h" //Sound bin file

//Art Data File includes
#include "Main_Char_Palette.h"
#include "Char_Top_Left.h"
#include "Char_Top_Right.h"
#include "Char_Bottom_Left.h"
#include "Char_Bottom_Right.h"
#include "Enemy_Palette.h"

//Memory Location definitions
#define MEM_VRAM ((volatile uint32*)0x06000000)
#define FIRST_BASE_BLOCK (0x6000000)
#define PLAYER_PALETTE (0x05000200)
#define PLAYER_BUFFER (0x6010000)
#define PLAYER_OAM_LOCATION (0x07000000)
#define ENEMY_BUFFER (0x6010060)
#define ENEMY_OAM_LOCATION (0x07000200)
#define DISPCNT_LOCATION (0x04000000)
#define BGCNT_LOCATION (0x04000008)
#define ENEMY_PAL (0x05000220)
#define BACKGROUND_COLOUR (0x05000000)

//Button identifiers
#define BUTTON_RIGHT (1<<4)
#define BUTTON_LEFT (1<<5)
#define BUTTON_DOWN (1<<7)
#define BUTTON_UP (1<<6)
#define BUTTON_BACKSPACE (1<<2)
#define BUTTON_ENTER (1<<3)

//OAM Attributes
#define ATTRO_YPOS 0
#define ATTRO_ROTATION 8
#define ATTRO_DISABLE 9
#define ATTRO_SHAPELOOKUP 14

#define ATTRO_XPOS 0
#define ATTRO_HORIZONTAL_FLIP 12
#define ATTRO_VERTICAL_FLIP 13
#define ATTRO_SIZELOOKUP 14

#define ATTRO_SPRITE_TILE 0
#define ATTRO_PRIORITY 10
#define ATTRO_PALETTE 12

//Variable Declarations

int updatedPlayerX; //Player x position that will be updated through input		
int updatedPlayerY; //Player y position that will be updated through input
//int gs; //Current game state. Used to switch between menus and main game states.



unsigned short* enemyOAM;

//Display control register pointer
unsigned short* DISPCNT;

//Pointer for the 4 BG layers
unsigned short* BGCNT;

//Pointer for the first character block
unsigned int* chars;

//Main char palette definition
unsigned short* palette;

//Defines character base block location
unsigned int* mychar;

unsigned short* playerOAM;

//Enemy Stuff - BROKEN :(
unsigned short* enemyPalette;

unsigned int* enemy;

//Background Color
unsigned short* backPalette;

unsigned short* mainDestTopLeft;
unsigned short* mainDestTopRight;
unsigned short* mainDestBottomLeft;
unsigned short* mainDestBottomRight;

unsigned short* enemyTopLeft;
unsigned short* enemyTopRight;
unsigned short* enemyBottomLeft;
unsigned short* enemyBottomRight;
	


struct player{
	
int playerXPos;
int playerYPos;
bool playerObjDisable;
int playerShapeLookup;
int playerSizeLookup;
int playerSpriteTile;
int playerPalette;
int playerPriority;
int updatedPlayerX;
int updatedPlayerY;
	
} player;

//Structure for enemies. Allows multiple enemies to be spawned
struct enemyChar{
	
	int offset; //An offset that is applied to the enemy memory address for each instance of an enemy.
	int enemyXPos; //Enemy x position on screen
	int enemyYPos; //Enemy y position on screen
	int enemyObjDisable; //Enemy OBJ disable flag
	int enemyShapeLookup; 
	int enemySizeLookup;
	int enemySpriteTile; //The sprite tile used by the enemy
	int enemyPriority; //Priority of the enemy sprites
	int enemyPaletteNum; //Enemy palette number being used
	
} enemyArray[3]; //Defines an array containing 3 enemies

	enum GameState{
	STATE_MAIN_MENU,
	STATE_RUNNING,
	STATE_PAUSE,
	STATE_GAME_OVER,
};


enum GameState gState;

//Function declarations

void drawBlock(unsigned char* source, unsigned short* dest); //Function that draws the image data read in from the source parameter
extern void SwapStore(unsigned short* source, unsigned short* dest[]);
extern void incrementNum(int* pos);
extern void decrementNum(int* pos);
extern void wrappingPositivePosition(int* comparison, int* pos);
extern void wrappingNegativePosition(int* comparison, int* pos, int* changeVal);
extern void drawBlockARM(unsigned char* source, unsigned short* dest);

//---------------------------------------------------------------------------------
// Program entry point
//---------------------------------------------------------------------------------
int main(void) {
//---------------------------------------------------------------------------------
 
enemyOAM = (unsigned short*)ENEMY_OAM_LOCATION; 

//Display control register pointer
DISPCNT = (unsigned short*)DISPCNT_LOCATION;

//Pointer for the 4 BG layers
BGCNT = (unsigned short*)BGCNT_LOCATION;

//Pointer for the first character block
chars = (unsigned int*)FIRST_BASE_BLOCK;

//Main char palette definition
palette = (unsigned short*)PLAYER_PALETTE;

//Defines character base block location
mychar = (unsigned int*)PLAYER_BUFFER;

playerOAM = (unsigned short*)PLAYER_OAM_LOCATION;

//Enemy palette pointer
enemyPalette = (unsigned short*)ENEMY_PAL;
//Enemy data pointer
enemy = (unsigned int*)ENEMY_BUFFER;

//Background Color
backPalette = (unsigned short*)BACKGROUND_COLOUR;

unsigned short* text = (unsigned short*)0x6004800;

int screen_Width = 240;
int screen_Height = 160;
int binaryFlip = 1;
int scoreCount = 0;


//Player OBJ data for drawing
mainDestTopLeft = (unsigned short*)&(mychar[1 * 8 + 0]);
mainDestTopRight = (unsigned short*)&(mychar[2 * 8 + 0]);
mainDestBottomLeft = (unsigned short*)&(mychar[33 * 8 + 0]);
mainDestBottomRight = (unsigned short*)&(mychar[34 * 8 + 0]);

//Player variable assigning
player.playerXPos = 1;
player.playerYPos = 1;
player.playerObjDisable = false;
player.playerShapeLookup = 0;
player.playerSizeLookup = 1;
player.playerSpriteTile = 1;
player.playerPalette = 0;
player.playerPriority = 1;
player.updatedPlayerX = 0;
player.updatedPlayerY = 0;

//Enemy OBJ data for drawing
enemyTopLeft = (unsigned short*) &(enemy[1 * 8 + 0]);
enemyTopRight = (unsigned short*) &(enemy[2 * 8 + 0]);
enemyBottomLeft = (unsigned short*) &(enemy[33 * 8 + 0]);
enemyBottomRight = (unsigned short*) &(enemy[34 * 8 + 0]);

//Assigning enemy variables
for(int n = 0; n < 3; n++){
	enemyArray[n].offset = n;

	enemyArray[n].enemyXPos = 70 + (n * 20);
	enemyArray[n].enemyYPos = 70;
	enemyArray[n].enemyObjDisable = false;
	enemyArray[n].enemyShapeLookup = 0;
	enemyArray[n].enemySizeLookup = 1;
	enemyArray[n].enemySpriteTile = 1;
	enemyArray[n].enemyPriority = 1;
	enemyArray[n].enemyPaletteNum = 1;
		
	}

gState = STATE_MAIN_MENU;


 
	// the vblank interrupt must be enabled for VBlankIntrWait() to work
	irqInit();
	//Sound set up - BROKEN
	irqSet( IRQ_VBLANK, mmVBlank );
	irqEnable(IRQ_VBLANK);

	// generic setup function
	consoleDemoInit();
	
	//Sound set up - BROKEN
	mmInitDefault( (mm_addr)soundbank_bin, 8 ); 
	
	//DISPCNT setup
	//bit 6 is for 1D OBJ data, 8 through 11 are the 4 layers
	//Bit 12 is the OBJ on/off switch
	DISPCNT[0] = ((0 << 0) | (0 << 6) | (1 << 8) | (1 << 9) | (1 <<10) | (1 << 11) | (1 << 12));
	
	//Bits 0 - 1 are the layer priority
	// Bits 2 - 3 are the character base block number
	//Bit 7 is colour mode
	//Bits 8 - 12 is the screen base block you use for the layer
	//Bits 14 - 15 are used for the screen size setting
	BGCNT[0] = ((2 << 0) | (0 << 2) | (0 << 7) | (10 << 8) | (0 << 14)) ; //Char block 0, priority 2
	BGCNT[1] = ((0 << 0) | (0 << 2) | (0 << 7) | (9 << 8) | (0 << 14)); //Looks at character block 1, priority 3
	BGCNT[2] = ((3 << 0) | (0 << 2) | (0 << 7) | (11 << 8) | (0 << 14)) ; //Char block 2, priority 0
	BGCNT[3] = ((1 << 0) | (3 << 2) | (0 << 7) | (20 << 8) |(0 << 14)); //Char block 3, priority 1
	
	//Blank character to go in first space of base block
	for(int n = 0; n < 8; n++){
		
		chars[n] = 0;
		
	}
	
	
	//text[2*32+2] = ((66<<0) | (0<< 10) | (0<<11) | (15 << 12));
	
	//Sets up player palette 
	for(int n = 0; n < 16; n++){
		
		//SwapStore(&palette[n], &main_Char_Palette[n]);
		palette[n] = main_Char_Palette[n];
	}
	
	//First line of my char set to 0 values
	mychar[0] = 0;
	
	//Character Sprite drawing
	drawBlockARM(Char_Top_Left_Bitmap, mainDestTopLeft);
	drawBlockARM(Char_Top_Right_Bitmap, mainDestTopRight);
	drawBlockARM(Char_Bottom_Left_Bitmap, mainDestBottomLeft);
	drawBlockARM(Char_Bottom_Right_Bitmap, mainDestBottomRight);
	
	//Sets up the player on screen with initial values
	playerOAM[1 * 4] = ((player.playerYPos << ATTRO_YPOS) | (player.playerObjDisable << ATTRO_DISABLE) | (player.playerShapeLookup << ATTRO_SHAPELOOKUP));
	playerOAM[(1 * 4) + 1] = ((player.playerXPos << ATTRO_XPOS)| (player.playerSizeLookup << ATTRO_SIZELOOKUP));
	playerOAM[(1 * 4) + 2] = ((player.playerSpriteTile << ATTRO_SPRITE_TILE) | (player.playerPalette << ATTRO_PRIORITY));
	
	//Sets up enemy palette
	for(int i = 0; i < 16; i++){
		//SwapStore(&enemyPalette[i], &enemy_Palette[i]);
		enemyPalette[i] = enemy_Palette[i];
	}
	
//Sets background colour 
	backPalette[0 * 16 + 0] = ((16 << 0) | (0 << 5) | (8 << 10));
	
	
	// main loop
	while (1) 
	{
		// ansi escape sequence to clear screen and home cursor
		// /x1b[line;columnH
		//iprintf("\x1b[");

		// ansi escape sequence to set print co-ordinates
		// /x1b[line;columnH
		//iprintf("\x1b[0;0H %d", num);
		scanKeys();
		
		mmStart( MOD_CT5PROGC, MM_PLAY_LOOP );
	
		
	unsigned short state = keysHeld();
	unsigned short pressedState = keysDown();
	

	
	
		switch (gState){
			case STATE_MAIN_MENU:
				//Main Menu
				text[7*32+8] = ((67<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[7*32+9] = ((111<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[7*32+10] = ((108<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[7*32+11] = ((100<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[7*32+12] = ((108<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[7*32+13] = ((105<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[7*32+14] = ((110<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[7*32+15] = ((101<<0) | (0<< 10) | (0<<11) | (15 << 12));
				
				text[11*32+8] = ((67<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[11*32+9] = ((97<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[11*32+10] = ((110<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[11*32+11] = ((97<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[11*32+12] = ((100<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[11*32+13] = ((97<<0) | (0<< 10) | (0<<11) | (15 << 12));
				
				if((BUTTON_ENTER) & pressedState){
				text[7*32+8] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[7*32+9] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[7*32+10] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[7*32+11] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[7*32+12] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[7*32+13] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[7*32+14] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[7*32+15] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				
				text[11*32+8] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[11*32+9] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[11*32+10] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[11*32+11] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[11*32+12] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[11*32+13] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				
					gState = STATE_RUNNING;
				}
			break;
			case STATE_RUNNING:
			
			//Main game loop
					//Right key
					if((BUTTON_RIGHT) & state){
						wrappingPositivePosition(&screen_Width, &player.updatedPlayerX);
						incrementNum(&player.updatedPlayerX);
					}
					
					//left key
					if((BUTTON_LEFT) & state){
						wrappingNegativePosition(&player.updatedPlayerX, &binaryFlip, &screen_Width);
						decrementNum(&player.updatedPlayerX);
						
					}
					
					//Down key
					if((BUTTON_DOWN) & state){
						wrappingPositivePosition(&screen_Height, &player.updatedPlayerY);
						incrementNum(&player.updatedPlayerY);
						
					}
					
					
					//up key
					if((BUTTON_UP) & state){
						wrappingNegativePosition(&player.updatedPlayerX, &binaryFlip, &screen_Height);
						decrementNum(&player.updatedPlayerY);
					}
					
					
					for (int n = 0; n < 2; n++){
						if(enemyArray[n].enemyXPos >= 240){
							
						enemyArray[n].enemyXPos = 1;
						
						}
						
						enemyArray[n].enemyXPos++;
					}
					
					for (int n = 2; n < 3; n++){
						if(enemyArray[n].enemyYPos >= 160){
							
						enemyArray[n].enemyYPos = 1;
						
						}
						
						enemyArray[n].enemyYPos++;
					}
					
					bool playerScored = false;
					
						for(int n = 0; n < 3; n++){
						if((player.updatedPlayerX + 8 >= enemyArray[n].enemyXPos ) && (player.updatedPlayerX - 8 <= enemyArray[n].enemyXPos)){
						if((player.updatedPlayerY + 8 >= enemyArray[n].enemyYPos ) && (player.updatedPlayerY - 8 <= enemyArray[n].enemyYPos)){
							if(enemyArray[n].enemyObjDisable == false){
								enemyArray[n].enemyObjDisable = true;
							
							playerScored = true;
							}
							
							}
						}
						}
						
						if(playerScored){
							scoreCount++;
						}
						
						if(scoreCount == 3){
							gState = STATE_GAME_OVER;
						}
				
					
					//Equivalent to a restart
					if((BUTTON_BACKSPACE) & pressedState){
						gState = STATE_MAIN_MENU;
					}
					
					if((BUTTON_ENTER) & pressedState){
						gState = STATE_PAUSE;
					}
			break;
				case STATE_PAUSE:
				//Pause
				text[9*32+11] = ((80<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[9*32+12] = ((97<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[9*32+13] = ((117<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[9*32+14] = ((115<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[9*32+15] = ((101<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[9*32+16] = ((100<<0) | (0<< 10) | (0<<11) | (15 << 12));
				
				if((BUTTON_ENTER) & pressedState){
					
				text[9*32+11] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[9*32+12] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[9*32+13] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[9*32+14] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[9*32+15] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[9*32+16] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				
				gState = STATE_RUNNING;
					
				}
				break;
				
				case STATE_GAME_OVER:
				text[7*32+8] = ((89<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[7*32+9] = ((111<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[7*32+10] = ((117<<0) | (0<< 10) | (0<<11) | (15 << 12));
				
				text[8*32+8] = ((87<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[8*32+9] = ((105<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[8*32+10] = ((110<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[8*32+11] = ((33<<0) | (0<< 10) | (0<<11) | (15 << 12));
				
				if((BUTTON_ENTER) & pressedState){
					
				text[7*32+8] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[7*32+9] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[7*32+10] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				
				text[8*32+8] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[8*32+9] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[8*32+10] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
				text[8*32+11] = ((0<<0) | (0<< 10) | (0<<11) | (15 << 12));
						gState = STATE_MAIN_MENU;
					}
				
				break;
		
		}
			//Update player
			playerOAM[1 * 4] = ((player.updatedPlayerY << ATTRO_YPOS) | (player.playerObjDisable << ATTRO_DISABLE) | (player.playerShapeLookup << ATTRO_SHAPELOOKUP));
			playerOAM[(1 * 4) + 1] = ((player.updatedPlayerX << ATTRO_XPOS) | (player.playerSizeLookup << ATTRO_SIZELOOKUP));
			playerOAM[(1 * 4) + 2] = ((player.playerSpriteTile << ATTRO_SPRITE_TILE) | (player.playerPriority << ATTRO_PRIORITY));
			
			//Update enemies
			for(int n = 0; n < 3; n++){
			enemyOAM[enemyArray[n].offset * 4] = ((enemyArray[n].enemyYPos << ATTRO_YPOS) | (enemyArray[n].enemyObjDisable << ATTRO_DISABLE) | (enemyArray[n].enemyShapeLookup << ATTRO_SHAPELOOKUP));
			enemyOAM[(enemyArray[n].offset * 4) + 1] = ((enemyArray[n].enemyXPos << ATTRO_XPOS)| (enemyArray[n].enemySizeLookup << ATTRO_SIZELOOKUP));
			enemyOAM[(enemyArray[n].offset * 4) + 2] = ((enemyArray[n].enemySpriteTile << ATTRO_SPRITE_TILE) | (enemyArray[n].enemyPriority << ATTRO_PRIORITY) | (enemyArray[n].enemyPaletteNum << ATTRO_PALETTE));
				
			}
			
			
		
		VBlankIntrWait();
		mmFrame();
	}
}

//Function that draws bitmap data on screen
/*void drawBlock(unsigned char* source, unsigned short* dest){
	
	for(int n = 0; n < 2*8; n++){
	unsigned char p1 = *source++;
	unsigned char p2 = *source++;
	unsigned char p3 = *source++;
	unsigned char p4 = *source++;
	
	*dest++ = (p4<<12) | (p3<<8) |(p2<<4) | p1;
	}
}*/








