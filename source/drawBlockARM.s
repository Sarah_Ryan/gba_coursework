.THUMB				@ turn on thumb
.ALIGN  2			@ align code correctly for GBA
.GLOBL  drawBlockARM		@ name of function goes here



.THUMB_FUNC			@ we are about to declare a thumb function
drawBlockARM:				@ function start

push { r4-r7, lr }	@ push r4-r7 and link register onto stack. Your function might use these
					@ registers, so we need to preserve the values just in case!
					@ we don't need to worry about r0-r3 as it is assumed they will be regularly messed up anyway
	

@ the magic happens here!
@ r0-r3 will automatically contain the parameters sent when calling the function.

	mov r6, #16
Loop: ldrb r4, [r0] @Load in p1 and store in r4. No shifting needed
	add  r0, #1 	@Increment the source 
	ldrb r5, [r0] 	@Load in P2 to r5
	lsl r5, r5, #4 	@Left shift P2 by 4
	orr r4, r4, r5 	@Perform OR operation between p1 and p2
	add r0, #1 		@Increment source
	ldrb r5, [r0] 	@Load in P3 to r5. r5 not needed again after the Or so can be reused
	lsl r5, r5, #8 	@Left shift p3 by 8 
	orr r4, r4, r5 	@OR operation
	add r0, #1 		@Increment source
	ldrb r5, [r0] 	@Load in p4
	lsl r5, r5, #12 	@left shift p4 by 12
	orr r4, r4, r5 	@perform Or on r4 and r5.
	add r0, #1
	strh r4, [r1]
	add r1, #2
	sub r6, #1
	cmp r6, #0
	bne Loop
	
	

pop { r4-r7 }		@ pop first 4 values from stack back into r4-r7, and also
pop { r3 }			@ pop the next value from stack (stored value for lr) into some unused register, e.g. r3 -
					@ we cannot overwrite lr so we have to do it via a normal register
bx r3				@ "branch and exchange" (return) back to C, using the previous value for lr stored in r3
@ ==================================
